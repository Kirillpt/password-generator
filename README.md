This console application generate passwords. You can input how many passwords you want to generate, and then you can input lenght of the password. Program will print a list of passwords, uses a standart alphabet, you can change it in source file. In the future it will be print passwords in the file, and user will be able to chose an alphabet for a password.
##Build
`$ gpp -std=c++11 pass_gen.cpp -o pass_gen`
Your binary will be named like `pass_gen`.
##Run
Just double-click binary file, for example pass_gen, and go to GENERATE. :)
Enjoy coding.
