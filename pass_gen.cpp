#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;
const string kirillpt_string = "qwertypasdfghjkzxcbnmQWERTYPASDFGHJKZXCVBNM123456789";
string generate_password(const string &alphabet, const int password_lenght)
{
	string password = "";
	int alphabet_size = alphabet.size();
	for(int index = 0; index < password_lenght; ++index)
	{
		password += alphabet[rand() % alphabet_size];		
	}
    return password;
}
template<typename Output>
void print_password(const string &alphabet, const int password_lenght, Output &out)
{
	out << generate_password(alphabet, password_lenght);
}
template<typename Output>
void print_input_number_of_passwords(Output &out)
{
	out << "Input number of passwords: ";
}
template<typename Number, typename Input, typename Output, typename Text_m>
void input_number_of_passwords(Number &number_of_passwords,Input &in,
								Output &out, Text_m text_mode )
{
	if(text_mode)
		print_input_number_of_passwords(out);
	in >> number_of_passwords;
}
template <typename Output>
void print_input_password_lenght(Output &out)
{
	out << "Input lenght of password: ";
}
template<typename Lenght, typename Input, typename Output, typename Text_m>
void input_password_lenght(Lenght &password_lenght, Input &in, Output &out,const Text_m &text_mode)
{
	if(text_mode)
		print_input_password_lenght(out);
	in >> password_lenght;
}
template <typename Output> 
void print_after_password(Output &out)
{
	out << "\n";
}
template <typename Output>
void print_few_passwords(Output &out,string &alphabet, int number_of_passwords, int password_lenght){
	for(int counter = 0; counter < number_of_passwords; ++counter)
	{
        print_password(alphabet,password_lenght,out);
        print_after_password(out);
	}
}

template <typename Output>
void generate_cf_logins(Output &out, int number_of_passwords, int password_lenght){
    // for codeforces
    string cnum = "279826";
    string div = " | ";
    for(int i = 0; i < number_of_passwords; ++i){
        string postf = "";
        string login = "gcup";
        string number = to_string(i);
        while(number.size() < 3)
            number = "0" + number;
        login += number;
        string pass = generate_password(kirillpt_string, password_lenght);
        if(i <= 40){
            postf = login + ", gymn29"; 
        }
        else{
            postf = login;
        }
        cout << cnum << div << login << div << pass << div << postf << '\n';
    }

}
int main(int argc, char* argv[]){
	srand(time(NULL));
	bool text_mode = 0;
	int number_of_passwords=1, password_lenght=8;
	string alphabet = kirillpt_string;
    if(argc == 1){
        input_number_of_passwords(number_of_passwords,cin,cout, text_mode);
        input_password_lenght(password_lenght,cin,cout,text_mode);
    }
    else if(argc == 2){
        number_of_passwords = stoi(argv[1]);
        input_password_lenght(password_lenght,cin,cout,text_mode);

    }
    else if(argc >= 3){
        number_of_passwords = stoi(argv[1]);
        password_lenght = stoi(argv[2]);
    }
    generate_cf_logins(cout, number_of_passwords, password_lenght);
}
